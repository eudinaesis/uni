# VBA07 -- Projekt: Schleifen

### Aufgabe: Schreibe ein Programm, das die ersten N Primzahlen berechnet und in ein Arbeitsblatt einfügt.

Es sollte den Wert für **N** aus Zelle `B1` nehmen; `A1:A3` sollte daher eine Titelzeile bilden, "Erste / N / Primzahlen". Es sollte die ersten **N** Primzahlen in die Zeilen der Spalte `C` einträgt, beginnend mit Zelle `C2`. Er sollte auch 1 bis **N** in Spalte `B` schreiben, beginnend mit `B2`. Die Ausgabe sollte wie folgt aussehen:

<img title="" src="erste-100-primzahlen.png" alt="erste-100-primzahlen.png" width="203" data-align="inline">

### Erste Schritte

Öffne eine neue Excel-Datei und speichere sie als Nachname_Schleifen_Primzahlen ab (Achtung: .xlsm nicht vergessen!)

Öffne den VBA-Editor mit Alt-F11 (oder klicke auf die Schaltfläche in den Entwicklertools).
Füge ein Modul ein (Einfügen → Modul) und und deklariere eine Prozedur (z.B. `Sub Primzahlen()`) .
**Vorschlag:** Füge das Direktfenster und das Lokal-Fenster (Ansicht) hinzu. Ordne die Fenster so an, dass du alles auf einmal sehen kannst, etwa so: 

![vba-editor-windows.png](vba-editor-windows.png)

**Vorschlag:** Öffne ein Webbrowser-Fenster und gehe zu https://vba-tutorial.de -- so kannst du dann per Alt-Tab sehr einfach zwischen einer VBA-Referenzseite und deiner Arbeit wechseln.

### Das Problem durchdenken

Primzahlen sind ganze Zahlen größer als 1, die durch keine andere ganze Zahl außer eins und sich selbst gleichmäßig teilbar sind. Zum Beispiel: 2, 3, 5, 7, 11, 13, 17...
Wie kannst du für eine beliebige ganze Zahl **P** prüfen, ob sie eine Primzahl ist oder nicht? Wenn die Zahl sehr groß ist, gibt es keine schnelle Möglichkeit zu testen, aber mit Hilfe von Schleifen kannst du prüfen, ob **P** durch eine andere Zahl **D** geteilt werden kann.

Dazu brauchst du den modularen Divisionsoperator `Mod`. `X Mod Y` gibt den Rest an, wenn man **X** durch **Y** dividiert, und 0, wenn es keinen Rest gibt (wenn **X** oder **Y** keine ganzen Zahlen sind, werden sie zuerst gerundet). Also:

```vba
'Beispiele X Mod Y

Dim MyResult
MyResult = 10 Mod 5    ' Returns 0.
MyResult = 10 Mod 3    ' Returns 1.
MyResult = 12 Mod 4.3    ' Returns 0.
MyResult = 12.6 Mod 5    ' Returns 3.
```

Man braucht mindestens zwei Schleifen: eine, die über mögliche Primzahlen iteriert, bis alle **N** Primzahlen gefunden sind, und eine, die innerhalb der ersten Schleife läuft und über mögliche Teiler iteriert.

Welche Art von Schleife würde hier am besten funktionieren?

- `For ... Next` funktioniert am besten, wenn du im Voraus weißt, wie oft du etwas tun willst

- `Do While ... Loop` und `Do Until ... Loop` funktionieren am besten, wenn du etwas **nur so lange** tun willst, wie eine Bedingung wahr (oder falsch) ist -- `Do While X = True` ist das gleiche wie `Do Until X = False`

- `Do ... Loop While` und `Do ... Loop Until` funktionieren am besten, wenn du etwas **mindestens einmal** machen willst, egal was, und es **nur dann weiter machen** willst, wenn eine Bedingung wahr (oder falsch) ist -- auch hier sind `Do ... Loop While X = True` und `Do ... Loop Until X = False` diesselbe

### Variablen

Welche Variablen wirst du brauchen und wo wirst du sie ändern?

Wahrscheinlich brauchst du mindestens eine `Integer`-Variable für N, die Anzahl der Primzahlen, die du finden willst; eine weitere für die Anzahl der Primzahlen, die du bereits gefunden hast; und eine weitere für die Zahl, die du gerade testest. Innerhalb der Schleife, die jede mögliche Primzahl testet, brauchst du wahrscheinlich noch eine `Integer`-Variable für den Divisor, durch den du sie zu teilen versuchst. Außerdem möchtest du vielleicht eine oder zwei `Boolean`-Variablen anlegen, um die logischen Tests zu verfolgen.

Du solltest aber auch weitere Variablen anlegen, wenn sie für dich sinnvoll sind -- es gibt viele Möglichkeiten, dieses Problem zu lösen!

### Debuggen und Tests

Einige nützliche Werkzeuge, um Probleme mit Ihrem Code zu beheben, sind `Debug.Print`, das Durchlaufen des Codes und die Verwendung von Haltepunkten sowie die Beobachtung lokaler Variablen.

#### Debug.Print

Mit `Debug.Print X` wird das Ergebnis von X im Direktbereich-Fenster ausgedruckt. Das ist besonders nützlich für Schleifen: Mit `Debut.Print` kannst du jedes Mal sehen, was die Schleife *eigentlich* tut, was sich manchmal von dem unterscheidet, was du *beabsichtigt* hast!

Hier siehst du das Ergebnis, wenn du `Debug.Print` benutzt, um zu drucken, was in einer einfachen `For ... Next`-Schleife. `Debug.Print` erwartet einen String (Zeichenkette)-Argument, aber du kannst Strings und Zahlen mit dem Operator `&` kombinieren: `StrBeispiel = "Text und " & 12345 & " Zahlen"` gibt der Variablen StrBeispiel den Wert `"Text und 12345 Zahlen"`.

![debug-print-loop.png](/Users/pnorthup/Documents/inf-praktikum/loops-lesson/debug-print-loop.png)

#### Einzelschritt in die Prozedur & Lokal-Fenster

Mit F8 kannst du in das Programm "einsteigen". Halte F8 gedrückt, um eine Zeile nach der anderen zu durchlaufen - die Zeile, in der du dich befindest, wird gelb markiert. Du kannst auch mit Strg-F8 direkt an die Stelle springen, an der sich dein Cursor befindet. Während du dich "im" Programm befindest, kannst du den aktuellen Wert aller Variablen im Fenster "Lokal" sehen - im Screenshot unten siehst du, dass N derzeit 5 ist.

![debug-einzelschritt.png](/Users/pnorthup/Documents/inf-praktikum/loops-lesson/debug-einzelschritt.png)

#### Haltepunkte

Haltepunkte sind ein zusätzliches Werkzeug, um in das Programm "hineinzukommen". Du kannst einen Haltepunkt setzen, indem du `F9` drückst, das Menü `Debuggen` verwendest oder einfach auf die graue Spalte links neben dem Codefenster klickst (wo die roten Punkte und der gelbe Pfeil sind). Wenn du einen Haltepunkte gesetzt hast, hält das Programm automatisch an, wenn es diese Zeile erreicht - du musst nicht durch einzelne Zeilen gehen, um dorthin zu gelangen.

![haltepunkte.png](/Users/pnorthup/Documents/inf-praktikum/loops-lesson/haltepunkte.png)

### Anforderungen

Dein Programm muss Folgendes tun:

- Die ersten N Primzahlen korrekt berechnen und einfügen.
- Leistung: Es sollte schnell genug sein, um die ersten 100 Primzahlen ohne lange Wartezeiten auszugeben.
- Eingabeüberprüfung: Es sollte eine Fehlermeldung ausgeben, wenn der Benutzer etwas anderes als eine ganze Zahl in A1 eingibt.

### Bonus-Herausforderungen

Wenn du die Grundfunktionalität erreicht hast, versuche, die Leistung zu verbessern. Musst du wirklich alle möglichen Teiler testen, die kleiner als die mögliche Primzahl sind, oder nur einige? Diskutiere mit anderen Schülerinnen und Schülern oder suche im Internet nach Ideen, wenn dir nichts einfällt, wie du die Leistung verbessern kannst.
