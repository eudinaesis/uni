# Visual Basic for Applications (VBA)

### Cell-Manipulation

Es gibt mehrere Möglichkeiten eine Zelle aus der Exeltabelle im Code

`Cells(1,1)`beispielsweise steht für die Zelle `"A1"`, im Code werden
Zellen quasi als Nummernpaar dargestellt, in der Form `Cells(Zeile,
Spalte)`. Beide dieser Nummern können durch Variablen ersetzt werden. Auf
diese Art und Weise kann mit Hilfe von Schleifen eine gewisse Anzahl an
Zellen nacheinander bearbeitet werden.

Achtung: anders als der in der textuellen Adresse (`A1`) wird hier die
Zeile vor der Spalte geschrieben!!

Nun wissen wir, wie man eine Zelle anwählt, möchten wir aber den Wert
verändern reicht der obige Code nicht aus. Mit Hilfe von
`Cells(1,1).Value`und einer Zuweisung können wir den Inhalt der Zelle
verändern. Und mit z.B. `X = Cells(1,1).Value` kann man den Wert aus einer Zelle einer Variablen zuweisen.

```vba
Dim N, X as Integer
N = 10
Cells(1,1).Value = "Faktorielle Tabelle"
Cells(1,2).Value = N
X = Cells(1,2).Value 'X ist jetzt auch 10!
```

![cell manipulation image](cell_manipulation.PNG)

Weitere Beispiele findet ihr unter [Schleifendurchlauf für einen Zellenbereich \| Microsoft Learn](https://learn.microsoft.com/de-de/office/vba/excel/concepts/cells-and-ranges/looping-through-a-range-of-cells)

### Schleifen

Bei der Faktoriellen Funktion werden alle vorangegangenen Zahlen einer
Zahl N miteinander multipliziert. Dieses mathematische Problem kann sehr
einfach mit der Hilfe einer Schleife gelöst werden.

Es gibt verschiedene Arten in VBA Schleifen zu schreiben. Die beiden
gängigsten sind die For- und die While-Schleife. Also:

- `For ... Next` funktioniert am besten, wenn du im Voraus weißt, wie
  oft du etwas tun willst
  
  ```vba
  Dim N, Counter, Result as Integer
  N = 10
  Result = 1
  For Counter = 1 to N
      Result = Result * Counter
      Cells(Counter + 1, 3).Value = Result 'Beginnt mit C2
  Next Counter
  ```

- `Do While ... Loop` und `Do Until ... Loop` funktionieren am besten,
  wenn du etwas **nur so lange** tun willst, wie eine Bedingung wahr
  (oder falsch) ist \-- `Do While X = True` ist das gleiche wie
  `Do Until X = False`
  
  ```vba
  Dim N, Counter, Result as Integer
  N = 10
  Counter= 1
  Result = 1
  Do While Counter <= N
      Result = Result * Counter
      Cells(Counter + 1, 3).Value = Result 'Beginnt mit C2
      Counter = Counter + 1
  Loop
  ```

- `Do ... Loop While` und `Do ... Loop Until` funktionieren am besten,
  wenn du etwas **mindestens einmal** machen willst, egal was, und es
  **nur dann weiter machen** willst, wenn eine Bedingung wahr (oder
  falsch) ist \-- auch hier sind `Do ... Loop While X = True` und
  `Do ... Loop Until X = False` diesselbe

### Debuggen und Tests

Einige nützliche Werkzeuge, um Probleme mit Ihrem Code zu beheben, sind
`Debug.Print`, das Durchlaufen des Codes und die Verwendung von
Haltepunkten sowie die Beobachtung lokaler Variablen.

#### Debug.Print

Mit `Debug.Print X` wird das Ergebnis von X im Direktbereich-Fenster
ausgedruckt. Das ist besonders nützlich für Schleifen: Mit `Debut.Print`
kannst du jedes Mal sehen, was die Schleife *eigentlich* tut, was sich
manchmal von dem unterscheidet, was du *beabsichtigt* hast!

Hier siehst du das Ergebnis, wenn du `Debug.Print` benutzt, um zu
drucken, was in einer einfachen `For ... Next`-Schleife. `Debug.Print`
erwartet einen String (Zeichenkette)-Argument, aber du kannst Strings
und Zahlen mit dem Operator `&` kombinieren:
`StrBeispiel = "Text und " & 12345 & " Zahlen"` gibt der Variablen
StrBeispiel den Wert `"Text und 12345 Zahlen"`.

![](debug-print-loop.png)

#### Einzelschritt in die Prozedur & Lokal-Fenster

Mit F8 kannst du in das Programm \"einsteigen\". Halte F8 gedrückt, um
eine Zeile nach der anderen zu durchlaufen - die Zeile, in der du dich
befindest, wird gelb markiert. Du kannst auch mit Strg-F8 direkt an die
Stelle springen, an der sich dein Cursor befindet. Während du dich
\"im\" Programm befindest, kannst du den aktuellen Wert aller Variablen
im Fenster \"Lokal\" sehen - im Screenshot unten siehst du, dass N
derzeit 5 ist.

![](debug-einzelschritt.png)

#### Haltepunkte

Haltepunkte sind ein zusätzliches Werkzeug, um in das Programm
\"hineinzukommen\". Du kannst einen Haltepunkt setzen, indem du `F9`
drückst, das Menü `Debuggen` verwendest oder einfach auf die graue
Spalte links neben dem Codefenster klickst (wo die roten Punkte und der
gelbe Pfeil sind). Wenn du einen Haltepunkte gesetzt hast, hält das
Programm automatisch an, wenn es diese Zeile erreicht - du musst nicht
durch einzelne Zeilen gehen, um dorthin zu gelangen.

![](haltepunkte.png)
