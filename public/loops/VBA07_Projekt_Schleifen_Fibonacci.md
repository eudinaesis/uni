# VBA07 -- Projekt: Schleifen -- Fibonacci-Zahlen

### Aufgabe: Schreibe ein Programm, das die ersten **N** Zahlen der Fibonacci-Folge berechnet und sie in ein Arbeitsblatt einträgt.

Die Fibonacci-Folge ist eine Reihe ganzer Zahlen, beginnend mit {0, 1}, bei der die nächste Zahl gleich der Summe der beiden vorherigen Zahlen ist. Diese Folge kommt in einer Vielzahl von Naturphänomenen vor und hat viele interessante mathematische Eigenschaften; siehe [Fibonacci-Folge - Wikipedia](https://de.wikipedia.org/wiki/Fibonacci-Folge) für weitere Informationen.

![fibonacci_sequence.png](fibonacci_sequence.png)

Es sollte den Wert für **N** aus Zelle `B1` nehmen; `A1:A3` sollte daher eine Titelzeile bilden, "Erste / N / Fibonacci-Zahlen". Es sollte die ersten **N** Fibonacci-Zahlen in die Zeilen der Spalte `C` einträgt, beginnend mit Zelle `C2`. Er sollte auch 1 bis **N** in Spalte `B` schreiben, beginnend mit `B2`. Die Ausgabe sollte wie folgt aussehen:

![erste-100-fibonacci.png](erste-100-fibonacci.png)

### Erste Schritte

Öffne eine neue Excel-Datei und speichere sie als Nachname_Schleifen_Fibonacci ab (Achtung: .xlsm nicht vergessen!)

Öffne den VBA-Editor mit Alt-F11 (oder klicke auf die Schaltfläche in den Entwicklertools).
Füge ein Modul ein (Einfügen → Modul) und und deklariere eine Prozedur (z.B. `Sub Fibonacci()`) .
**Vorschlag:** Füge das Direktfenster und das Lokal-Fenster (Ansicht) hinzu. Ordne die Fenster so an, dass du alles auf einmal sehen kannst, etwa so: 

![vba-editor-windows.png](vba-editor-windows-fib.png)

**Vorschlag:** Öffne ein Webbrowser-Fenster und gehe zu https://vba-tutorial.de -- so kannst du dann per Alt-Tab sehr einfach zwischen einer VBA-Referenzseite und deiner Arbeit wechseln.

### Das Problem durchdenken

#### Input und Output

Zur Erinnerung: man verwendet `Cells(Zeile,Spalte).Value`, um den Wert einer Zelle abzurufen oder zu setzen. Achtung: Code unten nur als Beispiel!

```vba
'NUR BEISPIEL! SCHREIB NICHT IN DEINE LÖSUNG!
Dim n,i As Integer
n = 10
Cells(1,1).Value = n 'Die Zelle A1 hat jetzt den Wert 10
i = Cells(1,1).Value 'Die Variable i hat nun ebenfalls den Wert 10
```

#### Variablen

Obwohl du diese Aufgabe auch mit weniger Variablen lösen kannst, wirst du es wahrscheinlich hilfreich finden, mindestens fünf Zahlen-Variablen zu verwenden: eine für `N`, also die Anzahl der Fibonacci-Zahlen, die du berechnen wirst; eine für den Zähler in deiner Schleife; zwei für die vorherigen Fibonacci-Zahlen, die du addierst; und eine für das Ergebnis.

```vba
Dim N As [etwas]
Dim Zaehler As [Etwas]
Dim Erste As [Etwas]
Dim Zweite As [Etwas]
Dim Ergebnis As [etwas...]
```

Welchen Variablentyp sollten diese Variablen haben? In [Datentypen - Das VBA-Tutorial](https://vba-tutorial.de/variablen/datentypen.htm) findet man eine Übersicht darüber, welche Arten von Zahlen in welche Arten von Variablen passen.

#### Schleifen-Arten

Welche Art von Schleife würde hier am besten funktionieren?

- `For ... Next` funktioniert am besten, wenn du im Voraus weißt, wie oft du etwas tun willst

- `Do While ... Loop` und `Do Until ... Loop` funktionieren am besten, wenn du etwas **nur so lange** tun willst, wie eine Bedingung wahr (oder falsch) ist -- `Do While X = True` ist das gleiche wie `Do Until X = False`

- `Do ... Loop While` und `Do ... Loop Until` funktionieren am besten, wenn du etwas **mindestens einmal** machen willst, egal was, und es **nur dann weiter machen** willst, wenn eine Bedingung wahr (oder falsch) ist -- auch hier sind `Do ... Loop While X = True` und `Do ... Loop Until X = False` diesselbe

Was beschreibt am besten die Berechnung der ersten `N` Zahlen in einer Folge?

#### Sonderfälle

Erst ab der 3. Fibonacci-Zahl kann man die Folge auf dem "normalen" Weg $F_{n-2}+F_{n-1}=F_n$ berechnen, da die erste und zweite Zahl nicht genug Vorgänger haben, um sich zu addieren!

Das heißt, du brauchst eine Art `If ... End If`-Verzweigung, um zu prüfen, ob du dich im "normalen" Fall oder in den beiden "Sonderfällen" befindest.

### Debuggen und Tests

Vorschlag: Wenn deine Zahlen falsch ausfallen, verwende `Debug.Print` innerhalb deiner Schleife, um aufzuzeichnen, wie deine Variablen an jedem Punkt tatsächlich aussehen.

### Anforderungen

Dein Programm muss Folgendes tun:

- Die ersten **N** Fibonacci-Zahlen korrekt berechnen und einfügen.
- Eingabeüberprüfung: Es sollte eine Fehlermeldung ausgeben, wenn der Benutzer etwas anderes als eine ganze Zahl in A1 eingibt.
- Leistung: Es sollte mindestens die ersten 75 Zahlen berechnen können.

### Bonus-Herausforderungen

- Schreibe eine zweite Version deines Programms, die eine andere Art von Schleife verwendet (wenn man `for ... next` verwendete, verwende `Do While` oder `Do Until` usw.).

- Erkläre in deinen eigenen Worten, warum die Berechnung der Fibonacci-Zahlen nach der 93. oder 94. schwierig ist.

## Benotungsschema

0-8 Punkte, mit der Möglichkeit für 2 Bonuspunkte

- 1 Punkt für das richtige Einlesen des Wertes aus der Tabelle

- 1 Punkt für die richtige Strukturierung der Schleife selbst

- 2 Punkte für das korrekte Berechnen der Werte

- 1 Punkt für das korrekte Schreiben der Werte in das Tabellenblatt

- 1 Punkt für Kommentare

- 1 Punkt für die Behandlung von Fehlern/Eingaben

- 1 Punkt für die korrekte Verwendung von Variablen

- 1 Bonuspunkt je Bonus-Herausforderung

- 1 Bonuspunkt für eine 2. Version mit dem anderen Schleifentyp
