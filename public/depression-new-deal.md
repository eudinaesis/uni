---
title: "The Great Depression and the New Deal"
subtitle: "7d GSK"
author: Peter Northup & Eduard Marcu
date: 15.3.2021
---

# Lesson plan and resources

## Coming attractions (not required, setting the mood)

[Stories from the Great Depression](https://www.youtube.com/watch?v=TpfY8kh5lUw)

# Day 1: 15.3.2021

## Prior knowledge

[Mentimeter: Wordcloud](https://www.menti.com/mavbx4ebb9)

## Bank runs in cinema & history

"It's a Wonderful Life" (1946)

A "bank run" (Bankansturm) is when a bank's customers lose trust in the bank, and all demand their money back at once. Because banks use their deposits (Bankguthaben, Bankeinlagen, Depositen) to make long-term loans (Darlehen, Kredite), they only have a small fraction of the total amount available for use. To pay back all the deposits would mean demanding repayment of all of the loans, but the debtors (Schuldner) may not have the money yet, either. As a result, a sudden panic can result in a crisis, even if the bank is fundamentally solid.

Between the crash of the US stock market in 1929 and 1933, there were many, many bank runs. Many banks failed as a result. "It's a Wonderful Life", a classic movie from 1946, has one famous depiction of a bank run. It takes place in 1932. The main character George runs a "Savings and Loan" (~= Genossenschaftsbank) that makes home loans (Hypotheken). He and his wife Mary have saved $2000 for their honeymoon. "Old Man Potter" owns most of the businesses in town, including the other main bank.

Watch the scene and think about how the run is depicted. What *kind* of problem is it--economic, or social/moral? How is it solved? Answer in English if possible, but in German if you need to in order to express your ideas clearly.

*[Video and questions](https://forms.gle/5fwN5kxGpVhcaXvf8)*

[Subtitles - computer generated - English](https://eudinaesis.gitlab.io/uni/bank-run-en.html)

[Subtitles - computer generated - German](https://eudinaesis.gitlab.io/uni/bank-run-de.html)

### Discussion

## FDR & the New Deal

### The Great Depression: very, very bad

![Unemployment](unemployment-1930s-usa-uk-de.png)

![GDP](real-gdp-1930s-uk-usa-de.png)

### The New Deal: overview

Franklin Delano Roosevelt was elected in 1932, in the depths of the Great Depression, and took office March 1933, in the midst of another banking panic. He promised a “New Deal” for the American people. What did this mean in practice? Watch the video, using the subtitles and automatic translations if needed, and answer the questions below. If you can answer in English, please do, but feel free to use German if necessary. No need for full sentences -- short phrases and keywords are fine.

We (the student teachers) will provide feedback on your answers, and you will receive a copy of your answers by email.

*[Video and questions](https://forms.gle/vb9nAUWZt8EsS2qk6)*

[Subtitles - computer generated - English](https://eudinaesis.gitlab.io/uni/the-new-deal-en.txt)

[Subtitles - computer generated - German](https://eudinaesis.gitlab.io/uni/the-new-deal-de.txt)

### Discussion

## What worked?

[Mentimeter](https://www.menti.com/sjyfe6jg3x)

# Day 2: 16.3.2021

"There is general agreement that, just as the gold standard transmitted the recessionary impulses internationally, breaking the “golden fetters” (Eichengreen 1992) contributed to recovery." (Ritschl & Straumann 2010, 170)

![The Gold Standard & monetary policy](https://eudinaesis.gitlab.io/uni/core-econ-gold-standard-1930s.jpg)

["Systemzusammenbruch verhindert, an wirtschaftlicher Erholung gescheitert"](https://eudinaesis.gitlab.io/uni/heimberger-eichengreen-review.html)

[Digital worksheet (with text) here](https://forms.gle/Gm6RMHFVXHponkGN9)

