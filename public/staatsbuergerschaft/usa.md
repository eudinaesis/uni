---
title: "Staatsbürgerschaft: USA"
author: Peter Northup & Paula Nastasi
date: 16.6.2021
---

# Staatsbürgerschaft: USA

![https://tinyurl.com/staatsbuergerschaft-usa](staatsbuergerschaft-usa.png)

---

[Back](https://tinyurl.com/staatsbuergerschaft)

## Aufgaben

1. Lies die verlinkte(n) Quelle(n) und benutze dein Flipchart-Papier, um dir Notizen zu machen, die dir helfen, deinen Mitschülern Antworten auf die folgenden Fragen zu präsentieren. Arbeitszeit: 15 Minuten.

    - Wer ist automatisch ein Staatsbürger? Welche Möglichkeiten zur Erlangung der Staatsbürgerschaft gibt es in diesem Land?
    - Wann hat das amerikanische Staatsbürgerschaftssystem seine grundlegende Form angenommen? Warum?
    - Ist die Erhaltung einer doppelten Staatsbürgerschaft möglich? Falls ja, inwiefern?
    - Ist die Staatsbürgerschaft in diesem Land eher das Modell der "Zugehörigkeit zu einem Volk" oder eher das Modell der "demokratischen Gleichheit"? Warum?
    - Was sind die Unterschiede und Gemeinsamkeiten zu Österreich?

2. Fasse die wichtigsten Aspekte dieses Staatsbürgerschaftssystems für den Rest der Klasse zusammen - maximal zwei Minuten Präsentation.

## Quellen

[Staatsbürgerschaft: Länderprofile USA](https://www.bpb.de/gesellschaft/migration/laenderprofile/143983/staatsbuergerschaft)

[Wikipedia, 14. Zusatzartikel zur Verfassung der Vereinigten Staaten -- nur Vorwort](https://de.wikipedia.org/wiki/14._Zusatzartikel_zur_Verfassung_der_Vereinigten_Staaten)

[Wikipedia, Staatsbürgerschaft der Vereinigten Staaten -- überfliege nur!](https://de.wikipedia.org/wiki/Staatsbürgerschaft_der_Vereinigten_Staaten)