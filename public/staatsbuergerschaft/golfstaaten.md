---
title: "Staatsbürgerschaft: Golfstaaten"
author: Peter Northup & Paula Nastasi
date: 16.6.2021
---

# Staatsbürgerschaft: Golfstaaten

![https://tinyurl.com/staatsbuergerschaft-golf](staatsbuergerschaft-golf.png)

---

[Back](https://tinyurl.com/staatsbuergerschaft)

## Aufgaben

1. Lies die verlinkte(n) Quelle(n) und benutze dein Flipchart-Papier, um dir Notizen zu machen, die dir helfen, deinen Mitschülern Antworten auf die folgenden Fragen zu präsentieren. Arbeitszeit: 15 Minuten.

    - Wer ist automatisch ein Staatsbürger? Welche Möglichkeiten zur Erlangung der Staatsbürgerschaft gibt es in diesem Land?
    - Wie hat sich die Staatsbürgerschaft in diesem Land in der jüngeren Geschichte verändert?
    - Ist die Erhaltung einer doppelten Staatsbürgerschaft möglich? Falls ja, inwiefern?
    - Ist die Staatsbürgerschaft in diesem Land eher das Modell der "Zugehörigkeit zu einem Volk" oder eher das Modell der "demokratischen Gleichheit"? Warum?
    - Was sind die Unterschiede und Gemeinsamkeiten zu Österreich?

2. Fasse die wichtigsten Aspekte dieses Staatsbürgerschaftssystems für den Rest der Klasse zusammen - maximal zwei Minuten Präsentation.

## Quellen

[BPB, Staatsangehörigkeit, Nationalismus und die nicht-Assimilation von Arbeitsmigranten in den GCC-Staaten -- aber nicht die Infobox!](https://www.bpb.de/gesellschaft/migration/laenderprofile/150788/staatsangehoerigkeit)