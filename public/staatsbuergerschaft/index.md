---
title: "Staatsbürgerschaft"
author: Peter Northup & Paula Nastasi
date: 16.6.2021
---

# Staatsbürgerschaft

## Vorarbeit

Staatsbürgerschaft ist wieder einmal in den Nachrichten! Ihr solltet euch den folgenden kurzen Clip vor dem Unterricht ansehen.

[ZIB2 13.6: SPÖ und Grüne wollen rascher einbürgern](https://tvthek.orf.at/profile/ZIB-2-am-Sonntag/13890057/ZIB-2-am-Sonntag/14095468/SPOe-und-Gruene-wollen-rascher-einbuergern/14936789)

[ZIB1 12.6: Koalition bei Staatsbürgerschaft uneinig](https://tvthek.orf.at/profile/ZIB-1/1203/ZIB-1/14095332/Koalition-bei-Staatsbuergerschaft-uneinig/14935586)

## Gruppenarbeit: 15 Minuten Arbeitszeit

Fragen und Quellen für jede Gruppe unter den jeweiligen Links

[USA](usa.html)

[Deutschland](deutschland.html)

[Türkei](tuerkei.html)

[Golfstaaten](golfstaaten.html)

## Präsentationen: nur 2 Minuten pro Gruppe!

