---
title: "Arbeitsblatt Antworten: The Great Depression and the New Deal"
subtitle: "7d GSK"
author: Peter Northup & Eduard Marcu
date: 15/16.3.2021
---

# Links

[Online-Arbeitsblatt (mit Video)](https://forms.gle/vb9nAUWZt8EsS2qk6)

[Video auf YouTube](https://www.youtube.com/watch?v=0rjtOWn5mj0)

[Subtitles - computer generated - English](https://eudinaesis.gitlab.io/uni/the-new-deal-en.txt)

[Subtitles - computer generated - German](https://eudinaesis.gitlab.io/uni/the-new-deal-de.txt)

# Antworten aus dem Unterrichtsgespräch 16.3

1. Was waren die „drei Rs“ des New Deal und was bedeuteten sie?

    Erleichterung, Erholung, Reform
    
2. Was war der Unterschied zwischen dem ersten und dem zweiten New Deal?

    i: 1933-34: Umstrukturierung der Wirtschaft, Rettung des Bankensektors
    
    ii: 1935-38: Verbesserung der Ressourcennutzung, Entlastung der landwirtschaftlichen Betriebe, Arbeitsprogramme der Regierung

3. Was waren die Works Progress Administration und das Civilian Conservation Corps?

    Das Civilian Conservation Corps (CCC) war ein freiwilliges öffentliches Arbeitshilfsprogramm, das von 1933 bis 1942 in den Vereinigten Staaten für arbeitslose, unverheiratete Männer im Alter von 18 bis 25 Jahren durchgeführt und schließlich auf 17 bis 28 Jahre ausgeweitet wurde.
    
    Die Works Progress Administration war die größte Bundesbehörde der USA, die im Zuge des New Deal geschaffen wurde. Sie war als Arbeitsbeschaffungsbehörde für die Millionen Arbeitslosen während der Great Depression konzipiert. Sie beschäftigte auch Künstler und andere, um kulturelle Werke zu produzieren.

4. Was war das Social Security Act?

    Eine Art von Nationaler Pensionsversicherung

5. Was war die Tennessee Valley Authority?

    Eine große Reihe von Wasserkraftwerken und chemischen Anlagen, die während des 1. Weltkriegs gebaut wurden, versorgten weite Teile des ländlichen Südens mit Strom, die nie einen solchen hatten.

6. Was hat die Federal Deposit Insurance Corporation getan? Wie wäre die Situation in der Bank, die im Movieclip von „It's a Wonderful Life“ gezeigt wird, anders gewesen, wenn die FDIC bereits existiert hätte?

    Die FDIC versicherte alle Spareinlagen von der Bundesregierung versichert sind. Hätte es die FDIC im Jahr 1932 gegeben, hätte es keinen Ansturm auf Georges Bank gegeben, weil jeder darauf vertraut hätte, dass seine Ersparnisse sicher sind.
    
7. Was waren damals zwei Kritikpunkte am New Deal?

    i. "Würde die USA auf den Weg zum Sozialismus führen."
    ii. "Eine Verschwendung von Geld und Ressourcen."


8. Wie fasst der Erzähler die Gesamtwirkung des New Deal zusammen (ab ~ 3: 45)? Positiv, negativ oder eine Mischung aus beidem?

    Der Erzähler gibt eine gemischte Bewertung ab. "Heute einige Ökonomen und Historiker glauben, dass Roosevelts Einhaltung der New-Deal-Richtlinien die  Depressionen New Deal-Programme haben jedoch Millionen von Menschen dazu gebracht, für die Arbeit zu sorgen  wirtschaftliche Erleichterung für Familien in Schwierigkeiten, die die New-Deal-Initiativen ebenfalls ermöglichten für den Bau von Straßen Schulen Parks Krankenhäuser und viele andere Einrichtungen, die  wurden in der ganzen Nation benötigt".
    
9. Diese Videos wurden von einer Gruppe von Sozialkundelehrern im Westen Oklahomas, einer ländlichen und konservativen Gegend der USA, gemacht. Wie verändert dieses Wissen die Art und Weise, wie Sie die Informationen interpretieren?

    Wir sollten uns daran erinnern, dass die Geschichte - insbesondere die jüngere politische Geschichte - oft immer noch Gegenstand politischer Auseinandersetzungen ist. So wie das "Rote Wien" z.B. von Wiener Sozialdemokraten und oberösterreichischen Konservativen unterschiedlich erinnert wird. Es mag helfen, sich den New Deal als Amerikas "Rotes Wien" vorzustellen - an das sich die meisten mit Stolz erinnern, die Konservativen aber mit einer gewissen Ambivalenz.