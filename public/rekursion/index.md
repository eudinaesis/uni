---
title: "Rekursion mit Scratch & Python"
author: Peter Northup
date: 25.02.2022
---

# Rekursion und Faktorielle Funktionen mit Scratch & Python

Ein Projekt von Peter Northup, für Scratch *und* Python

## Aufgabe mit Scratch

### Rückblick: Rekursion in Scratch

In dieser Aufgabe wirst du die Rekursion nutzen, um ein mathematisches Problem zu lösen. Denke daran, dass Rekursion bedeutet, ein Problem in kleinere Teile zu zerlegen, indem du einen Block erstellst, der sich selbst wiederholt aufruft. Das bedeutet, dass ein rekursiver Block einen "Basisfall" haben muss, an dem der rekursive Block endet; einen "einfachen Schritt", der bei jedem rekursiven Aufruf eine Aktion ausführt; und den rekursiven Schritt, der den Block erneut auslöst, aber mit einer anderen Eingabe. Normalerweise ruft der rekursive Schritt den Block jedes Mal mit einer kleineren Eingabe auf, und der Basisfall hört auf, wenn er eine bestimmte Stufe erreicht hat.

Denke daran, dass du für die Rekursion in Scratch einen Block erstellen musst, der sich dann selbst aufruft. Siehe das Beispiel unten: Dieser Block zählt von seinem Ausgangswert abwärts, indem er sich selbst rekursiv aufruft, anstatt eine Schleife zu bilden.

![Rekursion in Scratch](recursion-scratch-example.png)

### Aufgabe: Faktorielle Funktionen

Du erstellst einen Block zur Berechnung von Faktorielle Funktionen (auch: Fakultäten). Die Faktorielle von *n*, geschrieben *n!*, ist eine besondere Funktion: Sie ist gleich dem Produkt aller natürlichen Zahlen, die gleich oder kleiner sind als *n*. Zum Beispiel:

![Beispiele vom Wikipedia.de](factorial-ex.svg)

Tipp: Du wirst die Werte der Zwischenberechnungen irgendwie speichern müssen.

### Zusatzaufgabe

Wie könntest du alle Ergebnisse, die dein Block berechnet, dauerhaft speichern, so dass er die bereits berechneten Werte nicht jedes Mal neu berechnen muss?

## Aufgabe mit Python

Schreibe nun eine Funktion in Python, um Faktorielle zu berechnen. Versuche zuerst, es so zu machen, wie du es in Scratch gemacht hast.

### Zusatszaufgabe

Dann versuche es, ohne die Zwischenwerte in Variablen zu speichern.

Tipp: Python-Funktionen können, anders als Scratch-Blöcke, direkt Werte "returnen".

# Lösungen

## Scratch

### Basislösung mit Variable

Der Schlüssel zur Lösung dieses Problems in Scratch liegt darin, den "rekursiven Schritt", der Factorial (n-1) aufruft, *vor* dem "einfachen Schritt", der die Variable auf ihren vorherigen Wert multipliziert mit der aktuellen Eingabe in Factorial setzt, zu platzieren.

Auf diese Weise stellst du sicher, dass die rekursiven Aufrufe bis hinunter zu 1, dem "Basisfall", gehen, bevor du die rekursive Leiter wieder nach oben gehst und die "einfachen Schritte" aufrufst, um die Werte nacheinander zu multiplizieren, bis du schließlich mit dem Ausgangswert multiplizierst.

Siehe das Beispiel unten: 

![Factorielle Lösung mit Variable Trigger](factorial-variable-trigger.png)

![Factorielle Lösung mit Variable, Block](factorial-variable.png)

### Zusatzaufgabe-Lösung: gespeicherte Werte in einer Liste

Indem man die Werte in eine Liste und nicht in eine einfache Variable einträgt, kann man die gesamte faktorielle Reihe speichern. Dann kann man den Faktorblock so modifizieren, dass er einen zweiten "Basisfall" hat: Wenn der Eingabewert kleiner als die Länge der Liste ist, bedeutet dies, dass der Wert bereits berechnet worden sein muss, so dass der Block ganz aufhören kann und der Wert direkt aus der Liste gelesen werden kann. Siehe unten:

![Factorielle Lösung mit Liste Trigger](factorial-list-trigger.png)

![Factorielle Lösung mit Liste, Block](factorial-list.png)

## Python

Um die grundlegende Scratch-Lösung in Python mit einer Variablen zu implementieren, muss man sie innerhalb der Funktion ausdrücklich als global referenzieren. Siehe unten:

    #Python Lösung mit globale Variable
    
    result = 0
    
    def factorial(n):
        global result
        if n < 2:
            result = 1
        else:
            factorial(n-1)
            result = (result * n)
    
    factorial_input = int(input("Welche Nummer? (>0)"))
    
    factorial(factorial_input)
    
    print(result)

Indem man den Rückgabewert der Funktion direkt verwendet, kann man die Unordnung der globalen Variablen vermeiden. Siehe die sauberere Version unten:

    #Python Lösung mit Return-Werte

    def factorial(n):
        if n < 2:
            return 1
        else:
            return (factorial(n-1) * n)
    
    factorial_input = int(input("Welche Nummer? (>0)"))
    
    print(factorial(factorial_input))
